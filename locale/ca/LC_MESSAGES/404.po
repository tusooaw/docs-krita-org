# Translation of docs_krita_org_404.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Josep Ma. Ferrer <txemaq@gmail.com>, 2019.
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: 404\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 15:03+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.0\n"

# skip-rule: t-sp_2p
#: ../../404.rst:None
msgid ""
".. image:: images/color_category/Kiki_cLUTprofiles.png\n"
"   :alt: Image of Kiki looking confused through books."
msgstr ""
".. image:: images/color_category/Kiki_cLUTprofiles.png\n"
"   :alt: Imatge del Kiki amb mirada confusa entre llibres."

#: ../../404.rst:5
msgid "File Not Found (404)"
msgstr "No s'ha trobat el fitxer (404)"

#: ../../404.rst:10
msgid "This page does not exist."
msgstr "Aquesta pàgina no existeix."

#: ../../404.rst:12
msgid "This might be because of the following:"
msgstr "Aquesta podria ser una de les causes següents:"

#: ../../404.rst:14
msgid ""
"We moved the manual from MediaWiki to Sphinx and with that came a lot of "
"reorganization."
msgstr ""
"S'ha mogut el manual des de MediaWiki a Sphinx i amb això hi ha hagut una "
"reorganització."

#: ../../404.rst:15
msgid "The page has been deprecated."
msgstr "La pàgina ha esdevingut obsoleta."

#: ../../404.rst:16
msgid "Or a simple typo in the url."
msgstr "O un error en el tecleig de l'URL."

#: ../../404.rst:18
msgid ""
"In all cases, you might be able to find the page you're looking for by using "
"the search in the lefthand navigation."
msgstr ""
"En qualsevol cas, trobareu la pàgina que cercàveu usant la cerca a la "
"navegació de l'esquerra."
