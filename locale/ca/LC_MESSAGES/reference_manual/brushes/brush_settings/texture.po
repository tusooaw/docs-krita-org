# Translation of docs_krita_org_reference_manual___brushes___brush_settings___texture.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-05 03:40+0200\n"
"PO-Revision-Date: 2019-05-07 15:23+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../reference_manual/brushes/brush_settings/texture.rst:0
#| msgid ".. image:: images/brushes/Krita_2_9_brushengine_texture_01.png"
msgid ".. image:: images/brushes/Krita_2_9_brushengine_texture_05.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_texture_05.png"

#: ../../reference_manual/brushes/brush_settings/texture.rst:0
#| msgid ".. image:: images/brushes/Krita_2_9_brushengine_texture_01.png"
msgid ".. image:: images/brushes/Krita_2_9_brushengine_texture_04.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_texture_04.png"

#: ../../reference_manual/brushes/brush_settings/texture.rst:0
msgid ".. image:: images/brushes/Krita_2_9_brushengine_texture_01.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_texture_01.png"

#: ../../reference_manual/brushes/brush_settings/texture.rst:0
msgid ".. image:: images/brushes/Krita_2_9_brushengine_texture_02.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_texture_02.png"

#: ../../reference_manual/brushes/brush_settings/texture.rst:0
msgid ".. image:: images/brushes/Krita_2_9_brushengine_texture_07.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_texture_07.png"

#: ../../reference_manual/brushes/brush_settings/texture.rst:0
msgid ".. image:: images/brushes/Krita_2_9_brushengine_texture_06.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_texture_06.png"

#: ../../reference_manual/brushes/brush_settings/texture.rst:1
msgid "The texture brush settings option in Krita."
msgstr "L'opció dels ajustaments del pinzell de textura al Krita."

#: ../../reference_manual/brushes/brush_settings/texture.rst:11
#: ../../reference_manual/brushes/brush_settings/texture.rst:16
#: ../../reference_manual/brushes/brush_settings/texture.rst:21
msgid "Texture"
msgstr "Textura"

#: ../../reference_manual/brushes/brush_settings/texture.rst:11
msgid "Patterns"
msgstr "Patrons"

#: ../../reference_manual/brushes/brush_settings/texture.rst:18
msgid ""
"This allows you to have textured strokes. This parameter always shows up as "
"two parameters:"
msgstr ""

#: ../../reference_manual/brushes/brush_settings/texture.rst:23
#: ../../reference_manual/brushes/brush_settings/texture.rst:46
msgid "Pattern"
msgstr "Patró"

#: ../../reference_manual/brushes/brush_settings/texture.rst:24
msgid "Which pattern you'll be using."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/texture.rst:26
msgid "The size of the pattern. 1.0 is 100%."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/texture.rst:27
msgid "Scale"
msgstr "Escala"

#: ../../reference_manual/brushes/brush_settings/texture.rst:30
msgid "How much a brush is offset, random offset sets a new per stroke."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/texture.rst:31
msgid "Horizontal Offset & Vertical Offset"
msgstr "Desplaçament horitzontal i Desplaçament vertical"

#: ../../reference_manual/brushes/brush_settings/texture.rst:34
msgid "Multiply"
msgstr "Multiplica"

#: ../../reference_manual/brushes/brush_settings/texture.rst:35
msgid ""
"Uses alpha multiplication to determine the effect of the texture. Has a soft "
"feel."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/texture.rst:37
msgid "Subtract"
msgstr "Sostreu"

#: ../../reference_manual/brushes/brush_settings/texture.rst:37
msgid ""
"Uses subtraction to determine the effect of the texture. Has a harsher, more "
"texture feel."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/texture.rst:39
msgid "Texturing mode"
msgstr "Mode de textura"

#: ../../reference_manual/brushes/brush_settings/texture.rst:42
msgid ""
"Cutoff policy will determine what range and where the strength will affect "
"the textured outcome."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/texture.rst:44
msgid "Disabled"
msgstr "Desactivat"

#: ../../reference_manual/brushes/brush_settings/texture.rst:45
msgid "doesn't cut off. Full range will be used."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/texture.rst:47
msgid "cuts the pattern off."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/texture.rst:49
msgid "Brush"
msgstr "Pinzell"

#: ../../reference_manual/brushes/brush_settings/texture.rst:49
msgid "Cuts the brush-tip off."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/texture.rst:51
msgid "Cutoff policy"
msgstr "Política de retall"

#: ../../reference_manual/brushes/brush_settings/texture.rst:54
msgid ""
"Cutoff is... the grayscale range that you can limit the texture to. This "
"also affects the limit takes by the strength. In the below example, we move "
"from the right arrow moved close to the left one, resulting in only the "
"darkest values being drawn. After that, three images with larger range, and "
"underneath that, three ranges with the left arrow moved, result in the "
"darkest values being cut away, leaving only the lightest. The last example "
"is the pattern without cutoff."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/texture.rst:56
msgid "Cutoff"
msgstr "Retall"

#: ../../reference_manual/brushes/brush_settings/texture.rst:59
msgid "Invert the pattern."
msgstr "Inverteix el patró."

#: ../../reference_manual/brushes/brush_settings/texture.rst:61
msgid "Invert Pattern"
msgstr "Inversió del patró"

#: ../../reference_manual/brushes/brush_settings/texture.rst:63
msgid "Brightness and Contrast"
msgstr "Brillantor i contrast"

#: ../../reference_manual/brushes/brush_settings/texture.rst:67
msgid ""
"Adjust the pattern with a simple brightness/contrast filter to make it "
"easier to use. Because Subtract and Multiply work differently, it's "
"recommended to use different values with each:"
msgstr ""

#: ../../reference_manual/brushes/brush_settings/texture.rst:69
msgid ".. image:: images/brushes/Krita_3_1_brushengine_texture_07.png"
msgstr ".. image:: images/brushes/Krita_3_1_brushengine_texture_07.png"

#: ../../reference_manual/brushes/brush_settings/texture.rst:72
msgid "Strength"
msgstr "Intensitat"

#: ../../reference_manual/brushes/brush_settings/texture.rst:74
msgid ""
"This allows you to set the texture to Sensors. It will use the cutoff to "
"continuously draw lighter values of the texture (making the result darker)"
msgstr ""

#: ../../reference_manual/brushes/brush_settings/texture.rst:77
msgid ".. image:: images/brushes/Krita_2_9_brushengine_texture_03.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_texture_03.png"

# skip-rule: t-acc_obe
#: ../../reference_manual/brushes/brush_settings/texture.rst:80
msgid ""
"`David Revoy describing the texture feature (old) <http://www.davidrevoy.com/"
"article107/textured-brush-in-floss-digital-painting>`_"
msgstr ""
"`En David Revoy ens descriu la característica de la textura (antic) <http://"
"www.davidrevoy.com/article107/textured-brush-in-floss-digital-painting>`_"
