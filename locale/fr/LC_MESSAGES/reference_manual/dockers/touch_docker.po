msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-02-27 02:39+0100\n"
"Last-Translator: KDE Francophone <kde-francophone@kde.org>\n"
"Language-Team: KDE Francophone <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 1.5\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"
"X-Language: fr_FR\n"
"X-Qt-Contexts: true\n"
"Generated-By: Babel 0.9.6\n"
"X-Source-Language: C\n"

#: ../../reference_manual/dockers/touch_docker.rst:1
msgid "Overview of the touch docker."
msgstr ""

#: ../../reference_manual/dockers/touch_docker.rst:10
msgid "Touch"
msgstr ""

#: ../../reference_manual/dockers/touch_docker.rst:10
msgid "Finger"
msgstr ""

#: ../../reference_manual/dockers/touch_docker.rst:10
msgid "Tablet UI"
msgstr ""

#: ../../reference_manual/dockers/touch_docker.rst:15
msgid "Touch Docker"
msgstr "Panneau tactile"

#: ../../reference_manual/dockers/touch_docker.rst:17
msgid ""
"The Touch Docker is a QML docker with several convenient actions on it. Its "
"purpose is to aid those who use Krita on a touch-enabled screen by providing "
"bigger gui elements."
msgstr ""

#: ../../reference_manual/dockers/touch_docker.rst:19
msgid "Its actions are..."
msgstr ""

#: ../../reference_manual/dockers/touch_docker.rst:22
msgid "Open File"
msgstr ""

#: ../../reference_manual/dockers/touch_docker.rst:22
msgid "Save File"
msgstr "Enregistrer le fichier"

#: ../../reference_manual/dockers/touch_docker.rst:22
msgid "Save As"
msgstr ""

#: ../../reference_manual/dockers/touch_docker.rst:24
msgid "Undo"
msgstr ""

#: ../../reference_manual/dockers/touch_docker.rst:24
msgid "Redo"
msgstr "Refaire"

#: ../../reference_manual/dockers/touch_docker.rst:26
msgid "Decrease Opacity"
msgstr "Réduire l'opacité"

#: ../../reference_manual/dockers/touch_docker.rst:28
msgid "Increase Opacity"
msgstr "Augmenter l'opacité"

#: ../../reference_manual/dockers/touch_docker.rst:30
msgid "Increase Lightness"
msgstr "Augmenter la luminosité"

#: ../../reference_manual/dockers/touch_docker.rst:32
msgid "Decrease Lightness"
msgstr "Diminuer la luminosité"

#: ../../reference_manual/dockers/touch_docker.rst:34
msgid "Zoom in"
msgstr ""

#: ../../reference_manual/dockers/touch_docker.rst:36
msgid "Rotate Counter Clockwise 15°"
msgstr ""

#: ../../reference_manual/dockers/touch_docker.rst:36
msgid "Reset Canvas Rotation"
msgstr ""

#: ../../reference_manual/dockers/touch_docker.rst:36
msgid "Rotate Clockwise 15°"
msgstr ""

#: ../../reference_manual/dockers/touch_docker.rst:38
msgid "Zoom out"
msgstr ""

#: ../../reference_manual/dockers/touch_docker.rst:40
msgid "Decrease Brush Size"
msgstr "Diminuer la taille de la brosse"

#: ../../reference_manual/dockers/touch_docker.rst:42
msgid "Increase Brush Size"
msgstr "Augmenter la taille de la brosse"

#: ../../reference_manual/dockers/touch_docker.rst:44
msgid "Delete Layer Contents"
msgstr ""
