msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-02-27 08:12+0100\n"
"Last-Translator: KDE Francophone <kde-francophone@kde.org>\n"
"Language-Team: KDE Francophone <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 1.5\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"
"X-Language: fr_FR\n"
"X-Qt-Contexts: true\n"
"Generated-By: Babel 0.9.6\n"
"X-Source-Language: C\n"

#: ../../<rst_epilog>:50
msgid ""
".. image:: images/icons/perspectivegrid_tool.svg\n"
"   :alt: toolperspectivegrid"
msgstr ""

#: ../../reference_manual/tools/perspective_grid.rst:1
msgid "Krita's perspective grid tool reference."
msgstr ""

#: ../../reference_manual/tools/perspective_grid.rst:11
msgid "Tools"
msgstr ""

#: ../../reference_manual/tools/perspective_grid.rst:11
#, fuzzy
#| msgid "Perspective Grid Tool"
msgid "Perspective"
msgstr "Grille de perspective"

#: ../../reference_manual/tools/perspective_grid.rst:11
msgid "Grid"
msgstr ""

#: ../../reference_manual/tools/perspective_grid.rst:16
msgid "Perspective Grid Tool"
msgstr "Grille de perspective"

#: ../../reference_manual/tools/perspective_grid.rst:18
msgid "|toolperspectivegrid|"
msgstr ""

#: ../../reference_manual/tools/perspective_grid.rst:22
msgid "Deprecated in 3.0, use the :ref:`assistant_perspective` instead."
msgstr ""

#: ../../reference_manual/tools/perspective_grid.rst:24
msgid ""
"The perspective grid tool allows you to draw and manipulate grids on the "
"canvas that can serve as perspective guides for your painting. A grid can be "
"added to your canvas by first clicking the tool in the tool bar and then "
"clicking four points on the canvas which will serve as the four corners of "
"your grid."
msgstr ""

#: ../../reference_manual/tools/perspective_grid.rst:27
msgid ".. image:: images/tools/Perspectivegrid.png"
msgstr ""

#: ../../reference_manual/tools/perspective_grid.rst:28
msgid ""
"The grid can be manipulated by pulling on any of its four corners. The grid "
"can be extended by clicking and dragging a midpoint of one of its edges. "
"This will allow you to expand the grid at other angles. This process can be "
"repeated on any subsequent grid or grid section. You can join the corners of "
"two grids by dragging one onto the other. Once they are joined they will "
"always move together, they cannot be separated. You can delete any grid by "
"clicking on the red X at its center. This tool can be used to build "
"reference for complex scenes."
msgstr ""

#: ../../reference_manual/tools/perspective_grid.rst:30
msgid "As displayed while the Perspective Grid tool is active: *"
msgstr ""

#: ../../reference_manual/tools/perspective_grid.rst:33
msgid ".. image:: images/tools/Multigrid.png"
msgstr ""

#: ../../reference_manual/tools/perspective_grid.rst:34
msgid "As displayed while any other tool is active: *"
msgstr ""

#: ../../reference_manual/tools/perspective_grid.rst:37
msgid ".. image:: images/tools/KritaPersgridnoedit.png"
msgstr ""

#: ../../reference_manual/tools/perspective_grid.rst:38
msgid ""
"You can toggle the visibility of the grid from the main menu :menuselection:"
"`View --> Show Perspective Grid` option. You can also clear any grid setup "
"you have and start over by using the :menuselection:`View --> Clear "
"Perspective Grid`."
msgstr ""
