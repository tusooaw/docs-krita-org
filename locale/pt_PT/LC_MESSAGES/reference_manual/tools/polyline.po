# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-02-26 08:45+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: polylinetool icons Krita image polygontool images alt\n"
"X-POFile-SpellExtra: ref toolpolyline\n"

#: ../../<rst_epilog>:32
msgid ""
".. image:: images/icons/polyline_tool.svg\n"
"   :alt: toolpolyline"
msgstr ""
".. image:: images/icons/polyline_tool.svg\n"
"   :alt: ferramenta da linha poligonal"

#: ../../reference_manual/tools/polyline.rst:1
msgid "Krita's polyline tool reference."
msgstr "Referência da ferramenta da linha poligonal do Krita."

#: ../../reference_manual/tools/polyline.rst:10
msgid "Tools"
msgstr "Ferramentas"

#: ../../reference_manual/tools/polyline.rst:10
#| msgid "Polyline Tool"
msgid "Polyline"
msgstr "Linha Poligonal"

#: ../../reference_manual/tools/polyline.rst:15
msgid "Polyline Tool"
msgstr "Ferramenta da Linha Poligonal"

#: ../../reference_manual/tools/polyline.rst:17
msgid "|toolpolyline|"
msgstr "|toolpolyline|"

#: ../../reference_manual/tools/polyline.rst:20
msgid ""
"Polylines are drawn like :ref:`polygon_tool`, with the difference that the "
"double-click indicating the end of the polyline does not connect the last "
"vertex to the first one."
msgstr ""
"As linhas poligonais são desenhadas como a :ref:`polygon_tool`, com a "
"diferença que o duplo-click que indica o fim da linha poligonal não liga o "
"último vértice ao primeiro."
