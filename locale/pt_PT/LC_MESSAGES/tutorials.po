msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-02-19 14:08+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-POFile-SpellExtra: Krita\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: ../../tutorials.rst:5
msgid "Tutorials and Howto's"
msgstr "Tutoriais e HOWTO's"

#: ../../tutorials.rst:7
msgid ""
"Learn through developer and user generated tutorials to see Krita in action."
msgstr ""
"Aprenda com tutoriais gerados pelos programadores e utilizadores para ver o "
"Krita em acção."

#: ../../tutorials.rst:9
msgid "Contents:"
msgstr "Conteúdo:"
