msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-02-19 14:09+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-POFile-SpellExtra: Krita\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: ../../user_manual/introduction_from_other_software.rst:5
msgid "Introduction Coming From Other Software"
msgstr "Introdução Para Quem Vem de Outras Aplicações"

#: ../../user_manual/introduction_from_other_software.rst:7
msgid ""
"Krita is not the only digital painting application in the world. Because we "
"know our users might be approaching Krita with their experience from using "
"other software, we have made guides to illustrate differences."
msgstr ""
"O Krita não é a única aplicação de pintura digital no mundo. Por sabermos "
"que os nossos utilizadores poderão chegar ao Krita com a sua experiência "
"adquirida noutras aplicações, criámos alguns guias que ilustram as "
"diferenças."

#: ../../user_manual/introduction_from_other_software.rst:10
msgid "Contents:"
msgstr "Conteúdo:"
