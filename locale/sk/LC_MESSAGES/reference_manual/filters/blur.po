# translation of docs_krita_org_reference_manual___filters___blur.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___filters___blur\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-04-02 10:26+0200\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 18.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../<rst_epilog>:1
#, fuzzy
#| msgid ".. image:: images/en/Lens-blur-filter.png"
msgid ".. image:: images/filters/Lens-blur-filter.png"
msgstr ".. image:: images/en/Lens-blur-filter.png"

#: ../../reference_manual/filters/blur.rst:1
msgid "Overview of the blur filters."
msgstr ""

#: ../../reference_manual/filters/blur.rst:10
#: ../../reference_manual/filters/blur.rst:15
#: ../../reference_manual/filters/blur.rst:39
msgid "Blur"
msgstr "Rozmazať"

#: ../../reference_manual/filters/blur.rst:10
#: ../../reference_manual/filters/blur.rst:25
msgid "Gaussian Blur"
msgstr "Gaussovo rozmazanie"

#: ../../reference_manual/filters/blur.rst:10
msgid "Filters"
msgstr ""

#: ../../reference_manual/filters/blur.rst:17
msgid ""
"The blur filters are used to smoothen out the hard edges and details in the "
"images. The resulting image is blurry. below is an example of a blurred "
"image. The image of Kiki on right is the result of blur filter applied to "
"the image on left"
msgstr ""

#: ../../reference_manual/filters/blur.rst:21
#, fuzzy
#| msgid ".. image:: images/en/Blur.png"
msgid ".. image:: images/filters/Blur.png"
msgstr ".. image:: images/en/Blur.png"

#: ../../reference_manual/filters/blur.rst:22
msgid "There are many different filters for blurring:"
msgstr ""

#: ../../reference_manual/filters/blur.rst:27
msgid ""
"You can input the horizontal and vertical radius for the amount of blurring "
"here."
msgstr ""

#: ../../reference_manual/filters/blur.rst:30
#, fuzzy
#| msgid ".. image:: images/en/Gaussian-blur.png"
msgid ".. image:: images/filters/Gaussian-blur.png"
msgstr ".. image:: images/en/Gaussian-blur.png"

#: ../../reference_manual/filters/blur.rst:32
msgid "Motion Blur"
msgstr "Motion Blur"

#: ../../reference_manual/filters/blur.rst:34
msgid ""
"Doesn't only blur, but also subtly smudge an image into a direction of the "
"specified angle thus giving a feel of motion to the image. This filter is "
"often used to create effects of fast moving objects."
msgstr ""

#: ../../reference_manual/filters/blur.rst:37
#, fuzzy
#| msgid ".. image:: images/en/Motion-blur.png"
msgid ".. image:: images/filters/Motion-blur.png"
msgstr ".. image:: images/en/Motion-blur.png"

#: ../../reference_manual/filters/blur.rst:41
msgid "This filter creates a regular blur"
msgstr ""

#: ../../reference_manual/filters/blur.rst:44
#, fuzzy
#| msgid ".. image:: images/en/Blur-filter.png"
msgid ".. image:: images/filters/Blur-filter.png"
msgstr ".. image:: images/en/Blur-filter.png"

#: ../../reference_manual/filters/blur.rst:46
msgid "Lens Blur"
msgstr "Lens Blur"

#: ../../reference_manual/filters/blur.rst:48
msgid "Lens Blur Algorithm."
msgstr ""
