# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-06 03:34+0200\n"
"PO-Revision-Date: 2019-05-04 11:36+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: vänsterknapp"

#: ../../<rst_epilog>:6
msgid ""
".. image:: images/icons/Krita_mouse_middle.png\n"
"   :alt: mousemiddle"
msgstr ""
".. image:: images/icons/Krita_mouse_middle.png\n"
"   :alt: mittenknapp"

#: ../../<rst_epilog>:80
msgid ""
".. image:: images/icons/pan_tool.svg\n"
"   :alt: toolpan"
msgstr ""
".. image:: images/icons/pan_tool.svg\n"
"   :alt: panoreringsverktyg"

#: ../../reference_manual/tools/pan.rst:1
msgid "Krita's pan tool reference."
msgstr ""

#: ../../reference_manual/tools/pan.rst:11
msgid "Tools"
msgstr "Verktyg"

#: ../../reference_manual/tools/pan.rst:11
msgid "Pan"
msgstr "Panorera"

#: ../../reference_manual/tools/pan.rst:16
msgid "Pan Tool"
msgstr "Panoreringsverktyg"

#: ../../reference_manual/tools/pan.rst:18
msgid "|toolpan|"
msgstr "|panoreringsverktyg|"

#: ../../reference_manual/tools/pan.rst:20
msgid ""
"The pan tool allows you to pan your canvas around freely. It can be found at "
"the bottom of the toolbox, and you just it by selecting the tool, and doing |"
"mouseleft| + drag over the canvas."
msgstr ""

#: ../../reference_manual/tools/pan.rst:22
msgid ""
"There are two hotkeys associated with this tool, which makes it easier to "
"access from the other tools:"
msgstr ""

#: ../../reference_manual/tools/pan.rst:24
msgid ":kbd:`Space +` |mouseleft| + drag over the canvas."
msgstr ""

#: ../../reference_manual/tools/pan.rst:25
msgid "|mousemiddle| + drag over the canvas."
msgstr ""

#: ../../reference_manual/tools/pan.rst:27
msgid "For more information on such hotkeys, check :ref:`navigation`."
msgstr ""
