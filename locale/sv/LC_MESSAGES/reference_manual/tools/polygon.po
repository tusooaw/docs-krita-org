# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 11:22+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: vänsterknapp"

#: ../../<rst_epilog>:30
msgid ""
".. image:: images/icons/polygon_tool.svg\n"
"   :alt: toolpolygon"
msgstr ""
".. image:: images/icons/polygon_tool.svg\n"
"   :alt: polygonverktyg"

#: ../../reference_manual/tools/polygon.rst:1
msgid "Krita's polygon tool reference."
msgstr "Referens för Kritas polygonverktyg."

#: ../../reference_manual/tools/polygon.rst:10
msgid "Tools"
msgstr "Verktyg"

#: ../../reference_manual/tools/polygon.rst:10
msgid "Polygon"
msgstr "Polygon"

#: ../../reference_manual/tools/polygon.rst:15
msgid "Polygon Tool"
msgstr "Polygonverktyg"

#: ../../reference_manual/tools/polygon.rst:17
msgid "|toolpolygon|"
msgstr "|toolpolygon|"

#: ../../reference_manual/tools/polygon.rst:19
msgid ""
"With this tool you can draw polygons. Click the |mouseleft| to indicate the "
"starting point and successive vertices, then double-click or press :kbd:"
"`Enter` to connect the last vertex to the starting point."
msgstr ""
"Det går att rita polygoner med det här verktyget. Klicka på |mouseleft| för "
"att indikera startpunkten och följande knutpunkter, dubbelklicka därefter "
"eller tryck på :kbd:`Enter` för att ansluta den sista knutpunkten till "
"startpunkten."

#: ../../reference_manual/tools/polygon.rst:21
msgid ":kbd:`Shift + Z` undoes the last clicked point."
msgstr ":kbd:`Shift + Z` ångrar senast klickade punkt."

#: ../../reference_manual/tools/polygon.rst:24
msgid "Tool Options"
msgstr "Verktygsalternativ"
