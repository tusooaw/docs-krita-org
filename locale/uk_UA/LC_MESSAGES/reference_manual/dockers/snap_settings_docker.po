# Translation of docs_krita_org_reference_manual___dockers___snap_settings_docker.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___dockers___snap_settings_docker\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 16:20+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../<generated>:1
msgid "Guides"
msgstr "Напрямні"

#: ../../reference_manual/dockers/snap_settings_docker.rst:1
msgid "Overview of the snap settings docker."
msgstr "Огляд бічної панелі параметрів прилипання."

#: ../../reference_manual/dockers/snap_settings_docker.rst:11
msgid "Snap"
msgstr "Прилипання"

#: ../../reference_manual/dockers/snap_settings_docker.rst:16
msgid "Snap Settings"
msgstr "Параметри прилипання"

#: ../../reference_manual/dockers/snap_settings_docker.rst:20
msgid ""
"This docker has been removed in Krita 3.0. For more information on how to do "
"this instead, consult the :ref:`snapping page <snapping>`."
msgstr ""
"Цю бічну панель було вилучено у Krita 3.0. Щоб дізнатися більше про її "
"замінники, ознайомтеся із :ref:`розділом щодо прилипання <snapping>`."

#: ../../reference_manual/dockers/snap_settings_docker.rst:23
msgid ".. image:: images/dockers/Krita_Snap_Settings_Docker.png"
msgstr ".. image:: images/dockers/Krita_Snap_Settings_Docker.png"

#: ../../reference_manual/dockers/snap_settings_docker.rst:24
msgid ""
"This is docker only applies for Vector Layers. Snapping determines where a "
"vector shape will snap. The little number box is for snapping to a grid."
msgstr ""
"Ця бічна панель стосується лише векторних шарів. Прилипання визначає, до "
"чого прилипатиме векторна форма. Невеличке поле для введення чисел "
"призначено для визначення параметрів прилипання до ґратки."

#: ../../reference_manual/dockers/snap_settings_docker.rst:26
msgid "Node"
msgstr "Вузол"

#: ../../reference_manual/dockers/snap_settings_docker.rst:27
msgid "For snapping to other vector nodes."
msgstr "Для прилипання до інших вузлів векторних контурів."

#: ../../reference_manual/dockers/snap_settings_docker.rst:28
msgid "Extensions of Line"
msgstr "Продовження лінії"

#: ../../reference_manual/dockers/snap_settings_docker.rst:29
msgid ""
"For snapping to a point that could have been part of a line, had it been "
"extended."
msgstr ""
"Для прилипання до точки точка може бути частиною лінії або її продовження."

#: ../../reference_manual/dockers/snap_settings_docker.rst:30
msgid "Bounding Box"
msgstr "Обмежувальна рамка"

#: ../../reference_manual/dockers/snap_settings_docker.rst:31
msgid "For snapping to the bounding box of a vector shape."
msgstr "Для прилипання до обмежувальної рамки векторної форми."

#: ../../reference_manual/dockers/snap_settings_docker.rst:32
msgid "Orthogonal"
msgstr "Ортогональне"

#: ../../reference_manual/dockers/snap_settings_docker.rst:33
msgid "For snapping to only horizontal or vertical lines."
msgstr "Для прилипання до лише горизонтальних або лише вертикальних ліній."

#: ../../reference_manual/dockers/snap_settings_docker.rst:34
msgid "Intersection"
msgstr "Перетин"

#: ../../reference_manual/dockers/snap_settings_docker.rst:35
msgid "for snapping to other vector lines."
msgstr "Для прилипання до інших векторних ліній."

#: ../../reference_manual/dockers/snap_settings_docker.rst:37
msgid "Guides don't exist in Krita, therefore this one is useless."
msgstr "У Krita напрямних не передбачено, тому цей пункт не має сенсу."
