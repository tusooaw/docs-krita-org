msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-05 22:27\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_general_concepts___file_formats___file_ora.pot\n"

#: ../../general_concepts/file_formats/file_ora.rst:1
msgid "The Open Raster Archive file format as exported by Krita."
msgstr "Krita 导出的开放栅格图像归档文件格式。"

#: ../../general_concepts/file_formats/file_ora.rst:10
msgid "*.ora"
msgstr ""

#: ../../general_concepts/file_formats/file_ora.rst:10
msgid "ORA"
msgstr ""

#: ../../general_concepts/file_formats/file_ora.rst:10
msgid "Open Raster Archive"
msgstr ""

#: ../../general_concepts/file_formats/file_ora.rst:15
msgid "\\*.ora"
msgstr "\\*.ora"

#: ../../general_concepts/file_formats/file_ora.rst:17
msgid ""
".ora, or the Open Raster format, is an interchange format. It was designed "
"to replace :ref:`file_psd` as an interchange format, as the latter isn't "
"meant for that. Like :ref:`file_kra` it is loosely based on the Open "
"Document structure, thus a zip file with a bunch of xmls and pngs, but where "
"Krita's internal file format can sometimes have fully binary chunks, .ora "
"saves its layers as :ref:`file_png` making it fully open and easy to support."
msgstr ""
".ora，或者说开放栅格图像格式，是一种用于在不同程序间交换图像数据的文件格式。"
"它旨在替代 :ref:`file_psd`，因为后者并不是为了交换数据这个目的设计的。和 :"
"ref:`file_kra` 一样，ORA 格式大致基于 Open Document 文件结构，所以它本质上是"
"一个包含了一组 XML 和 PNG 文件的 ZIP 文件。它使用 :ref:`file_png` 而非 Krita "
"那样通过二进制数据编码图像，此举是为了保持文件格式的开放性和易支持性。"

#: ../../general_concepts/file_formats/file_ora.rst:19
msgid ""
"As an interchange format, it can be expected to be heavy and isn't meant for "
"uploading to the internet."
msgstr "该文件格式用于数据交换，体积较大，不适合用来在互联网上分享作品。"

#: ../../general_concepts/file_formats/file_ora.rst:23
msgid "`Open Raster Specification <https://www.openraster.org/>`_"
msgstr "`开放栅格图像格式规范 <https://www.openraster.org/>`_"
